SHELL := /bin/bash

all: get-sql init-db install set-domain

get-sql:
	source ./.env && curl $$SQL_DUMP_ADDRESS -o ./ocb.sql

init-db:
	docker-compose up -d db
	until docker-compose exec db sh -c "mysql -u\$$MYSQL_USER -p\$$MYSQL_PASSWORD \$$MYSQL_DATABASE -e 'select 1'"; do echo "Awaiting for Mysql"; sleep 5; done
	docker-compose exec db sh -c "mysql -u\$$MYSQL_USER -p\$$MYSQL_PASSWORD \$$MYSQL_DATABASE < /tmp/ocb.sql "; 

install:
	docker-compose up -d --build
	git submodule update --init
	docker-compose exec -u 0:0 app chown -R www-data /var/www/html/
	docker-compose exec app sh -c "php bin/console db:configure --db-host=db --db-name=\$$MYSQL_DATABASE --db-user=\$$MYSQL_USER --db-password=\$$MYSQL_PASSWORD -n"

set-domain:
	docker-compose exec app sh -c "php bin/console glpi:config:set -c 'core' 'url_base' '\$$DOMAIN'"
	docker-compose exec app sh -c "php bin/console glpi:config:set -c 'core' 'central_doc_url' '\$$DOMAIN/front/knowbaseitem.php'"
	docker-compose exec app sh -c "php bin/console glpi:config:set -c 'core' 'url_base_api' '\$$DOMAIN/apirest.php/'"
