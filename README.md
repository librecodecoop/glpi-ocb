# glpi-ocb

## Requisitos

- make
- curl
- docker
- docker-compose

## Instalação

- criar um arquivo `.env` com bse no arquivo `.env.dist` na raiz do projeto, informando os valores correspondentes;
- na raiz do projeto, rodar `make`

Após a instalação, o GLPI de estar rodando no dominio e porta informados no `.env`.
